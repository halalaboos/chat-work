package net.halalaboos.chatwork.utils;

public final class StringUtils {

	private StringUtils() {
		
	}
	
	/**
	 * Gives you everything after 'x' words in a sentence.
	 * <br>
	 * EX: getEverythingAfter("this is an interesting sentence", 2)
	 * <br>
	 * Returns 'an interesting sentence'
	 * */
	public static String getAfter(String text, int index) {
		String[] words = text.split(" ");
		if (words.length < index)
			return null;
		int splitIndex = 0;
		for (int i = 0; i < index; i++)
			splitIndex += words[i].length() + 1;
		return text.substring(splitIndex);
	}
	
	public static String getBefore(String text, int index) {
		String[] words = text.split(" ");
		if (words.length < index)
			return null;
		int splitIndex = 0;
		for (int i = 0; i < index; i++)
			splitIndex += words[i].length() + 1;
		return text.substring(0, splitIndex);
	}
	
	public static String replaceIgnoreCase(String input, String regex, String replacement) {
        return input.replaceAll("(?i)" + regex, replacement);
    }
	
	public static String keepSafe(String input) {
		input = input.replaceAll(" ", "_");
		if (input.length() > 24)
			input = input.substring(0, 24);
		return input;
	}
	
	public static int toHash(String first, String last) {
	    return 37 * first.hashCode() + last.hashCode();
	}
	
	/**
	 * @return true if the text could be parsed as an integer.
	 * */
	public static boolean isInteger(String text) {
		try {
			Integer.parseInt(text);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	/**
	 * @return true if the text could be parsed as a double.
	 * */
	public static boolean isDouble(String text) {
		try {
			Double.parseDouble(text);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	/**
	 * @return true if the text could be parsed as a double.
	 * */
	public static boolean isFloat(String text) {
		try {
			Float.parseFloat(text);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
}
