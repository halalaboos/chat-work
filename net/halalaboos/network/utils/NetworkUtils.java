package net.halalaboos.network.utils;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class NetworkUtils {

	public static void writeString(DataOutputStream outputStream, String text) throws IOException {
		char[] chars = text.toCharArray();
		outputStream.writeInt(chars.length);
		for (char c : chars) {
			outputStream.writeByte(c);
		}
	}
	
	public static String readString(DataInputStream inputStream) throws IOException {
		int length = inputStream.readInt();
		char[] text = new char[length];
		for (int i = 0; i < length; i++) {
			text[i] = (char) inputStream.readByte();
		}
		return String.valueOf(text);
	}
}
