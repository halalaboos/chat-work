package net.halalaboos.chatwork.protocol.packet;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import net.halalaboos.network.api.Handler;
import net.halalaboos.network.api.Packet;
import net.halalaboos.network.utils.NetworkUtils;

public class PacketMessage implements Packet<String> {

	@Override
	public String read(Handler handler, DataInputStream inputStream)
			throws IOException {
		String text = NetworkUtils.readString(inputStream);
		return text;
	}

	@Override
	public void write(Handler handler, String text,
			DataOutputStream outputStream) throws IOException {
		NetworkUtils.writeString(outputStream, text);
	}

}
