import java.io.IOException;
import java.net.UnknownHostException;

import javax.swing.UIManager;

import net.halalaboos.chatwork.Chatwork;
import net.halalaboos.chatwork.gui.Gui;
import net.halalaboos.chatwork.utils.WindowHelper;


public class Main {

	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}

		Gui.instance.setupFrame();
		String username = WindowHelper.askInput("Username", "Give username.");
		if (username != null && !username.isEmpty()) {
			Chatwork.instance.getInfo().setUsername(username);
		}
		try {
			String ip = WindowHelper.askInput("Connect to a server", "Give ip address.");
			if (ip != null && !ip.isEmpty()) {
				Chatwork.instance.connect(ip, 50);
			}
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	
	}

}
