package net.halalaboos.chatwork.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JTextField;

import net.halalaboos.chatwork.Chatwork;
import net.halalaboos.chatwork.protocol.data.UserInfo;
import net.halalaboos.chatwork.utils.WindowHelper;

public class GuiListener implements ActionListener {

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JTextField) {
			JTextField textField = (JTextField) e.getSource();
			if (!textField.getText().isEmpty()) {
				Chatwork.instance.write(0, textField.getText());
				textField.setText("");
			}
		} else if (e.getSource() instanceof JButton) {
			String username = WindowHelper.askInput("Username", "Give username.");
			if (username != null && !username.isEmpty()) {
				Chatwork.instance.setInfo(new UserInfo(username, "", "bumblebee46", "minecraft.net", 0));
			}
		}
	}

}
