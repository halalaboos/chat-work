package net.halalaboos.chatwork.protocol.data;

public class UserInfo {

	private String username, password, location, ingameUsername;
	
	private int permissions = 0;
	
	private boolean muted = false;
	
	public UserInfo(String username, String password, String location, String ingameUsername, int permissions) {
		this.username = username;
		this.password = password;
		this.location = location;
		this.ingameUsername = ingameUsername;
		this.permissions = permissions;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getIngameUsername() {
		return ingameUsername;
	}

	public void setIngameUsername(String ingameUsername) {
		this.ingameUsername = ingameUsername;
	}

	public int getPermissions() {
		return permissions;
	}

	public void setPermissions(int permissions) {
		this.permissions = permissions;
	}

	public boolean isMuted() {
		return muted;
	}

	public void setMuted(boolean muted) {
		this.muted = muted;
	}
	
}
