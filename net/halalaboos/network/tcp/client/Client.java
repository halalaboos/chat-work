package net.halalaboos.network.tcp.client;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import net.halalaboos.network.api.Handler;
import net.halalaboos.network.api.Packet;
import net.halalaboos.network.api.ProtocolHandler;
import net.halalaboos.network.api.listeners.PacketListener;

public class Client implements Handler {

	private Socket socket;
	
	private DataInputStream inputStream;
	
	private DataOutputStream outputStream;
	
	private ConnectionHandler connectionHandler;
	
	private final ProtocolHandler protocolHandler;
	
	private final List<PacketListener> packetListeners = new CopyOnWriteArrayList<PacketListener>();
	
	public Client(ProtocolHandler protocolHandler) {
		this.protocolHandler = protocolHandler;
	}
	
	public void connect(String ip, int port) throws UnknownHostException, IOException {
		socket = new Socket(ip, port);
		inputStream = new DataInputStream(socket.getInputStream());
		outputStream = new DataOutputStream(socket.getOutputStream());
		connectionHandler = new ConnectionHandler(this, socket);
		protocolHandler.setup();
		connectionHandler.start();
	}
	
	@Override
	public Socket getSocket() {
		return socket;
	}

	@Override
	public DataInputStream getInputStream() {
		return inputStream;
	}

	@Override
	public DataOutputStream getOutputStream() {
		return outputStream;
	}
	
	public boolean isConnected() {
		return socket == null ? false : socket.isClosed() ? false : socket.isConnected();
	}

	public ProtocolHandler getProtocolHandler() {
		return protocolHandler;
	}
	
	public ConnectionHandler getConnectionHandler() {
		return connectionHandler;
	}

	public void setConnectionHandler(ConnectionHandler connectionHandler) {
		this.connectionHandler = connectionHandler;
	}
	
	public void write(int id, Object data) {
		protocolHandler.write(id, data, this);
	}

	@Override
	public void onRecievedPacket(Packet packet, Object data) {
		for (PacketListener listener : packetListeners) {
			listener.onRecievedPacket(packet, data);
		}
	}

	public void disconnect() throws IOException {
		if (isConnected()) {
			socket.close();
		}
	}

	public void addPacketListener(PacketListener listener) {
		packetListeners.add(listener);
	}
	
	public void removePacketListener(PacketListener listener) {
		packetListeners.remove(listener);
	}

}
