package net.halalaboos.chatwork.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.io.File;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import net.halalaboos.chatwork.protocol.data.User;

public final class Gui {

	public static final Gui instance = new Gui();
	
	private static final GuiListener listener = new GuiListener();

	private static final JFrame frame = new JFrame();
	
	private static final JTextArea textArea = new JTextArea();
	
	private static final JTextField textField = new JTextField();
	
	private static final JList<User> userList = new JList<User>();
	
	private static final JButton changeButton = new JButton("Change info");
	
	private Gui() {
		
	}
	
	public void setupFrame() {
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle("Chatwork");
		setupComponents();
		frame.setSize(640, 480);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

	private void setupComponents() {
		frame.add(changeButton, BorderLayout.NORTH);
		changeButton.addActionListener(listener);
		frame.add(new JScrollPane(textArea), BorderLayout.CENTER);
		ImageIcon img = new ImageIcon(new File("./res/wings.png").getAbsolutePath());
		frame.setIconImage(img.getImage());
		textArea.setEditable(false);
		textArea.setBackground(Color.BLACK);
		textArea.setForeground(Color.GREEN);
		frame.add(textField, BorderLayout.SOUTH);
		textField.addActionListener(listener);
		textField.setDocument(new JTextFieldLimit(256));
		textField.setBackground(Color.BLACK);
		textField.setForeground(Color.GREEN);
		frame.add(userList, BorderLayout.EAST);
		userList.setBackground(Color.BLACK);
		userList.setForeground(Color.GREEN);
	}

	public void addLine(String line) {
		textArea.append(line + "\n");
	}
	
	public void updateUsers(List<User> users) {
		userList.setListData(users.toArray(new User[users.size()]));
	}
	
}
