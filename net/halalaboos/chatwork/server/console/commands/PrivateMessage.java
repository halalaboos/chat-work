package net.halalaboos.chatwork.server.console.commands;

import net.halalaboos.chatwork.server.ChatworkClient;
import net.halalaboos.chatwork.server.Server;
import net.halalaboos.chatwork.server.console.Command;
import net.halalaboos.chatwork.utils.StringUtils;

public class PrivateMessage implements Command {

	@Override
	public String getDescription() {
		return "Send a private message to another user.";
	}

	@Override
	public String[] getAliases() {
		return new String[] { "pm", "msg", "whisp", "whisper" };
	}

	@Override
	public boolean isValid(String input) {
		return input.contains(" ") ? input.split(" ").length >= 3 : false;
	}

	@Override
	public void execute(ChatworkClient sender, String input, String[] args) {
		String reciever = args[0];
		String message = StringUtils.getAfter(input, 2);
		ChatworkClient client = Server.getUser(reciever);
		client.sendMessage(sender.getUsername() + "->" + client.getUsername() + ": " + message);
		sender.sendMessage(sender.getUsername() + "->" + client.getUsername() + ": " + message);
	}

	@Override
	public void help(ChatworkClient sender) {
		sender.sendMessage("/pm <user> <message>");
	}

	@Override
	public int getPermission() {
		return 0;
	}

}
