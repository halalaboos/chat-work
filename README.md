Simple chatting protocol made for a Minecraft mod many moons ago.

I would not recommend using this to anyone.

* The connections are not encrypted
* The passwords are hashed without any magic seed and without any username
* The transfer of data is slow (if I recall)