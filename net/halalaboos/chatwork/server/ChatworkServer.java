package net.halalaboos.chatwork.server;

import java.io.File;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.halalaboos.chatwork.protocol.ChatworkProtocol;
import net.halalaboos.chatwork.protocol.data.User;
import net.halalaboos.chatwork.protocol.data.UserInfo;
import net.halalaboos.chatwork.protocol.packet.PacketUser;
import net.halalaboos.chatwork.protocol.packet.PacketMessage;
import net.halalaboos.chatwork.server.console.CommandManager;
import net.halalaboos.chatwork.utils.FileUtils;
import net.halalaboos.chatwork.utils.Hash;
import net.halalaboos.chatwork.utils.Logger;
import net.halalaboos.network.api.Packet;
import net.halalaboos.network.api.ProtocolHandler;
import net.halalaboos.network.tcp.server.ConnectionHandler;
import net.halalaboos.network.tcp.server.Server;

public class ChatworkServer extends Server <ChatworkClient> {

	public static final File USER_FILE = new File("./users.txt");
	
	private int idCount = 0;
	
	/**
	 * Key: username, Value: hashcode
	 * */
	private Map<String, String> users = new HashMap<String, String>();
	
	/**
	 * Key: username, Value: user data
	 * */
	private Map<String, UserInfo> userData = new HashMap<String, UserInfo>();
	
	public ChatworkServer(int port) throws IOException {
		super(port);
	}

	@Override
	public void start() {
		super.start();
		CommandManager.load();
		Bans.load();
		this.loadUsers();
	}
	
	@Override
	protected void onConnect(ChatworkClient handler) {
	}

	@Override
	protected void onDisconnect(ChatworkClient handler) {
		if (handler.isAuthenticated()) {
			this.getConnectionHandler().broadcast(4, handler);
			broadcast(handler.getUsername() + " has left!");
		}
	}
	
	@Override
	public void onRecievedPacket(ChatworkClient handler, Packet packet, Object data) {
		if (packet instanceof PacketMessage) {
			if (handler.isAuthenticated()) {
				if (((String) data).startsWith("/"))
					CommandManager.tryCommand(handler, ((String) data).substring(1));
				else {
					if (handler.throttleMessaging((String) data))
						handler.sendMessage("Slow down!");
					else if (handler.getInfo().isMuted())
						handler.sendMessage("You're muted!");
					else
						broadcast("[" + Permissions.getPrefix(handler.getInfo().getPermissions()) + "] " + handler.getUsername() + ": " + data);
				}
			} else
				handler.close();
		} else if (packet instanceof PacketUser) {
			UserInfo info = ((User) data).getInfo();
			if (!handler.isAuthenticated()) {
				handler.setInfo(info);
				if (!authenticate(handler, info)) {
					handler.sendMessage("Invalid login!");
					handler.close();
					return;
				}
				info.setPermissions(userData.get(info.getUsername()).getPermissions());
				info.setMuted(userData.get(info.getUsername()).isMuted());
				handler.setAuthenticated(true);
				handler.write(2, getAuthenticatedClients());
				for (ChatworkClient client : getAuthenticatedClients())
					if (client != handler && client.isAuthenticated())
						client.write(3, handler);
				broadcast(handler.getUsername() + " has joined!");
				broadcastUserInfo(handler);
			} else {
			    handler.getInfo().setLocation(info.getLocation());
			    handler.getInfo().setIngameUsername(info.getIngameUsername());
			    handler.getInfo().setUsername(info.getUsername());
				broadcastUserInfo(handler);
			}
		}
	}
	
	private boolean authenticate(ChatworkClient handler, UserInfo info) {
		try {
			if (users.containsKey(info.getUsername()))
				return Hash.validatePassword(info.getPassword(), users.get(info.getUsername()));
			else {
				users.put(info.getUsername(), Hash.createHash(info.getPassword()));
				userData.put(info.getUsername(), info);
				this.saveUsers();
				return true;
			}
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (InvalidKeySpecException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	@Override
	protected ConnectionHandler genConnectionHandler() {
		return new ChatworkHandler(this, this.getServerSocket());
	}

	@Override
	protected ProtocolHandler genProtocolHandler() {
		return new ChatworkProtocol();
	}
	
	public void broadcast(String message) {
		this.getConnectionHandler().broadcast(0, message);
		Logger.log("message", message);
	}
	
	public void broadcastUserInfo(User user) {
		this.getConnectionHandler().broadcast(1, user);
	}
	
	public void broadcastConnectedUsers() {
		this.getConnectionHandler().broadcast(2, getAuthenticatedClients());
	}
	
	public List<ChatworkClient> getAuthenticatedClients() {
		List<ChatworkClient> clients = new ArrayList<ChatworkClient>();
		for (ChatworkClient client : (List<ChatworkClient>) this.getConnectionHandler().getClients()) {
			if (client.isAuthenticated())
				clients.add(client);
		}
		return clients;
	}
	
	public void loadUsers() {
		users.clear();
		List<String> lines = FileUtils.readFile(USER_FILE);
		for (int i = 0; i < lines.size(); i++) {
			String line = lines.get(i);
			if (line.equals("USER")) {
				i++;
				String hash = lines.get(i);
				i++;
				String username = lines.get(i);
				i++;
				int permissions = Integer.parseInt(lines.get(i));
				i++;
				boolean muted = Boolean.parseBoolean(lines.get(i));
				UserInfo userInfo = new UserInfo(username, null, null, null, permissions);
				userInfo.setMuted(muted);
				userData.put(username, userInfo);
				users.put(username, hash);
			}
		}
	}
	
	public void saveUsers() {
		List<String> lines = new ArrayList<String>();
		Set<String> usernames = users.keySet();
		for (String username : usernames) {
			lines.add("USER");
			lines.add(users.get(username)/*hash*/);
			lines.add(username);
			lines.add("" + userData.get(username).getPermissions());
			lines.add("" + userData.get(username).isMuted());
		}
		FileUtils.writeFile(USER_FILE, lines);
	}
	
	public int nextId() {
		return idCount++;
	}
}
