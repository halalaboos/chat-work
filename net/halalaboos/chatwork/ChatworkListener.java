package net.halalaboos.chatwork;

import java.util.List;

import net.halalaboos.chatwork.gui.Gui;
import net.halalaboos.chatwork.protocol.data.User;
import net.halalaboos.chatwork.protocol.packet.PacketJoin;
import net.halalaboos.chatwork.protocol.packet.PacketLeave;
import net.halalaboos.chatwork.protocol.packet.PacketUser;
import net.halalaboos.chatwork.protocol.packet.PacketMessage;
import net.halalaboos.chatwork.protocol.packet.PacketPlayerList;
import net.halalaboos.network.api.Packet;
import net.halalaboos.network.api.listeners.PacketListener;

public class ChatworkListener implements PacketListener {
	
	@Override
	public void onRecievedPacket(Packet packet, Object data) {
		if (packet instanceof PacketMessage) {/*Message*/
			Gui.instance.addLine(data.toString());
		} else if (packet instanceof PacketPlayerList) {/*User list, provided when first join and users are online*/
			Gui.instance.updateUsers((List<User>) data);
		} else if (packet instanceof PacketUser) {/*User info*/
			User user = (User) data;
			Gui.instance.updateUsers(Chatwork.instance.getUsers());
		} else if (packet instanceof PacketJoin) {/*User joining, only id is provided*/
			User user = (User) data;
			Gui.instance.updateUsers(Chatwork.instance.getUsers());
		} else if (packet instanceof PacketLeave) {/*User leaving, only id is provided*/
			User user = (User) data;
			Gui.instance.updateUsers(Chatwork.instance.getUsers());
		}
	}

}
