package net.halalaboos.network.tcp.server;

import java.io.IOException;
import java.net.ServerSocket;

import net.halalaboos.network.api.Packet;
import net.halalaboos.network.api.ProtocolHandler;

public abstract class Server <T extends ClientHandler> {

	private final ServerSocket serverSocket;
	
	private final ConnectionHandler connectionHandler;
	
	private final ProtocolHandler protocolHandler;

	public Server(int port) throws IOException {
		serverSocket = new ServerSocket(port);
		this.protocolHandler = genProtocolHandler();
		this.connectionHandler = genConnectionHandler();
	}

	public void start() {
		protocolHandler.setup();
		connectionHandler.start();
	}
	
	public void close() throws IOException {
		serverSocket.close();
		connectionHandler.close();
	}
	
	public boolean isOnline() {
		return !serverSocket.isClosed();
	}
	
	protected abstract void onDisconnect(T handler);
	
	protected abstract void onConnect(T handler);

	protected abstract void onRecievedPacket(T handler, Packet packet, Object data);

	protected abstract ConnectionHandler genConnectionHandler();
	
	protected abstract ProtocolHandler genProtocolHandler();

	public ConnectionHandler getConnectionHandler() {
		return connectionHandler;
	}

	public ProtocolHandler getProtocolHandler() {
		return protocolHandler;
	}

	protected ServerSocket getServerSocket() {
		return serverSocket;
	}
	
}
