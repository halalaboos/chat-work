package net.halalaboos.chatwork;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import net.halalaboos.chatwork.protocol.ChatworkProtocol;
import net.halalaboos.chatwork.protocol.data.UserInfo;
import net.halalaboos.chatwork.protocol.data.User;
import net.halalaboos.chatwork.protocol.packet.PacketJoin;
import net.halalaboos.chatwork.protocol.packet.PacketLeave;
import net.halalaboos.chatwork.protocol.packet.PacketPlayerList;
import net.halalaboos.chatwork.protocol.packet.PacketPoke;
import net.halalaboos.chatwork.protocol.packet.PacketUser;
import net.halalaboos.network.api.Packet;
import net.halalaboos.network.tcp.client.Client;

public final class Chatwork extends Client implements User {

	public static final Chatwork instance = new Chatwork();
	
	private UserInfo info = new UserInfo("halalaboos", "jinglebells", "", "", 0);
	
	private List<User> users = new ArrayList<User>();
	
	private Chatwork() {
		super(new ChatworkProtocol());
		addPacketListener(new ChatworkListener());
	}
	
	@Override
	public void connect(String ip, int port) throws UnknownHostException, IOException {
		super.connect(ip, port);
		writeInfo();
	}
	
	private void writeInfo() {
		write(1, this);
	}

	public void sendMessage(String message) {
		write(0, message);
	}

	@Override
	public long getId() {
		return 0;
	}

	@Override
	public UserInfo getInfo() {
		return info;
	}
	
	
	public List<User> getUsers() {
		return users;
	}
	
	protected void setUsers(List<User> users) {
		this.users = users;
	}
	
	public User getUser(int id) {
		for (User user : users) {
			if (user.getId() == id)
				return user;
		}
		return null;
	}
	
	protected void updateUser(User user) {
		for (User user1 : users) {
			if (user1.getId() == user.getId()) {
				user1.setInfo(user.getInfo());
			}
		}
	}
	
	@Override
	public void onRecievedPacket(Packet packet, Object data) {
		if (packet instanceof PacketPlayerList) {
			setUsers((List<User>) data);
		} else if (packet instanceof PacketUser) {
			User user = (User) data;
			updateUser(user);
		} else if (packet instanceof PacketJoin) {
			User user = (User) data;
			join(user);
		} else if (packet instanceof PacketLeave) {
			User user = (User) data;
			leave(user);
		} else if (packet instanceof PacketPoke) {
			User user = getUser((Integer) data);
			if (user != null)
				poked(user);
		}
		super.onRecievedPacket(packet, data);
	}
	
	protected void join(User user) {
		users.add(user);
	}
	
	protected void leave(User user) {
		for (User user1 : users) {
			if (user1.getId() == user.getId()) {
				users.remove(user1);
				return;
			}
		}
	}
	
	protected void poked(User user) {
		
	}

	@Override
	public void setInfo(UserInfo info) {
		this.info = info;
		if (isConnected())
			writeInfo();
	}

}
