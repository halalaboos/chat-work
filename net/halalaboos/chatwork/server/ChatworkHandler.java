package net.halalaboos.chatwork.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import net.halalaboos.network.tcp.server.ConnectionHandler;
import net.halalaboos.network.tcp.server.Server;

public class ChatworkHandler extends ConnectionHandler <ChatworkClient> {

	private final ChatworkServer server;
	
	public ChatworkHandler(ChatworkServer server, ServerSocket serverSocket) {
		super(server, serverSocket);
		this.server = server;
		
	}
	
	@Override
	protected ChatworkClient generateHandler(Server server, Socket socket) throws IOException {
		return new ChatworkClient(server, socket);
	}

	@Override
	protected boolean shouldAllowConnect(ChatworkClient handler) {
		return !isConnected(handler) && !Bans.isBanned(handler.getAddress());
	}
	
	public boolean isConnected(ChatworkClient handler) {
		/*for (ChatworkClient otherHandler : this.getClients()) {
			if (handler != otherHandler && (handler.getInfo() == null ? handler.getAddress().equalsIgnoreCase(otherHandler.getAddress()) : handler.getUsername().equalsIgnoreCase(otherHandler.getUsername()))) {
				return true;
			}
		}*/
		return false;
	}

}
