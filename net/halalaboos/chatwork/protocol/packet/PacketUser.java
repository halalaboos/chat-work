package net.halalaboos.chatwork.protocol.packet;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import net.halalaboos.chatwork.protocol.data.Player;
import net.halalaboos.chatwork.protocol.data.User;
import net.halalaboos.chatwork.protocol.data.UserInfo;
import net.halalaboos.chatwork.utils.StringUtils;
import net.halalaboos.network.api.Handler;
import net.halalaboos.network.api.Packet;
import net.halalaboos.network.utils.NetworkUtils;

public class PacketUser implements Packet<User> {

	@Override
	public void write(Handler handler, User user,
			DataOutputStream outputStream) throws IOException {
		outputStream.writeInt((int) user.getId());
		NetworkUtils.writeString(outputStream, StringUtils.keepSafe(user.getInfo().getUsername()));
		NetworkUtils.writeString(outputStream, user.getInfo().getPassword());
		NetworkUtils.writeString(outputStream, user.getInfo().getLocation());
		NetworkUtils.writeString(outputStream, user.getInfo().getIngameUsername());
	}

	@Override
	public User read(Handler handler, DataInputStream inputStream)
			throws IOException {
		int id = inputStream.readInt();
		String username = StringUtils.keepSafe(NetworkUtils.readString(inputStream)),
				password = NetworkUtils.readString(inputStream),
				location = NetworkUtils.readString(inputStream),
				ingameUsername = NetworkUtils.readString(inputStream);
		return new Player(id, new UserInfo(username, password, location, ingameUsername, 0));
	}

}
