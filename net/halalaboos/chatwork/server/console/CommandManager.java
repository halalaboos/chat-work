package net.halalaboos.chatwork.server.console;

import java.util.ArrayList;
import java.util.List;

import net.halalaboos.chatwork.server.ChatworkClient;
import net.halalaboos.chatwork.server.console.commands.Ban;
import net.halalaboos.chatwork.server.console.commands.Help;
import net.halalaboos.chatwork.server.console.commands.Mute;
import net.halalaboos.chatwork.server.console.commands.PrivateMessage;
import net.halalaboos.chatwork.server.console.commands.Unmute;

public final class CommandManager {
	
	private static final List<Command> commands = new ArrayList<Command>();
	
	private CommandManager() {
		
	}
	
	public static void tryCommand(ChatworkClient sender, String input) {
		String commandAlias = input.contains(" ") ? input.split(" ")[0] : input;
		for (Command command : commands) {
			if (command.getPermission() > sender.getInfo().getPermissions())
				continue;
			for (String alias : command.getAliases()) {
				if (alias.equalsIgnoreCase(commandAlias)) {
					try {
						String[] args = input.contains(" ") ? input.substring(input.indexOf(" ") + 1).split(" ") : null;
						command.execute(sender, input, args);
					} catch (Exception e) {
						command.help(sender);
					}
					return;
				}
			}
		}
		sender.sendMessage("Invalid command!");
	}

	public static void load() {
		commands.add(new Help());
		commands.add(new PrivateMessage());
		commands.add(new Ban());
		commands.add(new Mute());
		commands.add(new Unmute());
	}

	public static List<Command> getCommands() {
		return commands;
	}
	
}
